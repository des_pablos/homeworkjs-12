const images = document.querySelectorAll('.image-to-show');

let intervalStatus = true;
let interval;
let counter = 0;

const stopSlide = document.querySelector('.stop-slide');
stopSlide.addEventListener('click', () => {
    clearInterval(interval);
    intervalStatus = true;
});

const playSlide = document.querySelector('.play-slide');
playSlide.addEventListener('click', showImgs);

function showImgs() {
    if (intervalStatus) {
        interval = setInterval(() => {
            images[counter].classList.remove('active');
            counter += 1;

            if (counter === images.length) {
                counter = 0;
            }
            images[counter].classList.add('active');
        }, 3000);
        intervalStatus = false;
    }
}

showImgs();
